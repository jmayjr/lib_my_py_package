#!/usr/bin/env python3
# -*- coding: utf-8 -*-


"""This module sets up the package for the lib_my_py_package"""

import pathlib
from setuptools import find_packages, setup

here = pathlib.Path(__file__).parent.resolve()

# Get the long description from the README file
long_description = (here / 'README.md').read_text(encoding='utf-8')

setup(
    name="lib_my_py_package",
    version="0.1.2",

    author="John May",
    author_email="",
    maintainer="John May",
    maintainer_email="",

    # url="https://git...git",  # add repo url
    # download_url="https://git...git",

    # add keywords to help find package in search
    keywords=['terminal'],
    license="MIT",
    description="Print 'Hello World' and echos an argument",
    long_description=long_description,
    long_description_content_type="text/markdown",
    # project_urls={
    #     "Bug Tracker": "https://git../issues",  # add url to issues
    # },
    python_requires=">=3.6",
    packages=find_packages(),

    # include_package_data=True,
    install_requires=[
        # 'pytest',
    ],
    classifiers=[
        'Environment :: Console',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Operating System :: POSIX :: Linux',
        'Programming Language :: Python :: 3'],
    entry_points={
        'console_scripts': [
            'hello = lib_my_py_package.__main__:main',
        ]},
    # setup_requires=['pytest-runner'],
    # tests_require=['pytest'],
)
